## Description

Simple application for managing you messages. Currently, via this api you can create new message and fetch existing
messages based on email address.

## Prerequisites

1. Running Cassandra. You can use docker for it:

```
docker run -p 9042:9042 --name cassandra cassandra:4
```

2. Keyspace and `message` table created:

```
CREATE KEYSPACE asseco WITH replication = {'class':'SimpleStrategy','replication_factor':1};
CREATE TABLE asseco.message (email TEXT, id UUID, content TEXT, title TEXT, PRIMARY KEY (email, id));
```

## How to use this api

1. Create new message

```
curl -X POST -H "Content-Type: application/json" \
-d "{\"title\": \"linuxize\", \"email\": \"linuxize@example.com\", \"content\": \"linuxize\"}" \
http://localhost:8080/api/messages
```

2. Find messages based on email

```
curl -X GET "http://localhost:8080/api/messages?email=linuxize@example.com" 
```
