package com.asseco.integration;

import com.asseco.domain.Message;
import com.asseco.domain.repository.MessageRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

class InMemoryMessageRepository implements MessageRepository {

    private static final List<Message> savedMessages = new ArrayList<>();

    @Override
    public UUID save(final Message message) {
        savedMessages.add(message);
        return message.id();
    }

    @Override
    public Stream<Message> findByEmail(final String email) {
        if (email.equals("spring.boot@gmail.com")) {
            return Stream.of(
                    new Message(UUID.fromString("0f97a38d-3708-409b-8dcd-f4f938eb60be"), email, "glory", "win"),
                    new Message(UUID.fromString("4ff36f85-a650-45b6-b954-8353b43a2583"), email, "level", "how")
            );
        } else {
            return Stream.empty();
        }
    }

    List<Message> savedMessages() {
        return savedMessages;
    }
}
