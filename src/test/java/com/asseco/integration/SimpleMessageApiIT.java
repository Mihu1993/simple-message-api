package com.asseco.integration;

import com.asseco.application.request.CreateMessageRequest;
import com.asseco.application.response.CreateMessageResponse;
import com.asseco.application.response.GetMessageByEmailResponse;
import com.asseco.domain.Message;
import com.asseco.domain.repository.MessageRepository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

import java.util.List;
import java.util.stream.Stream;

import static com.asseco.integration.SimpleMessageApiIT.SimpleMessageApiTestContext;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

@Import(SimpleMessageApiTestContext.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SimpleMessageApiIT {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private int port;

    @Autowired
    private InMemoryMessageRepository inMemoryMessageRepository;

    @ParameterizedTest
    @MethodSource("messagesByEmail")
    @DisplayName("should find messages based on email")
    void findMessages(final String email, final List<GetMessageByEmailResponse> responses) {
        // given
        // when
        final GetMessageByEmailResponse[] response = testRestTemplate.getForObject(
                "http://localhost:" + port + "/api/messages?email=" + email, GetMessageByEmailResponse[].class);

        // then
        assertThat(List.of(response)).isEqualTo(responses);
    }

    @Test
    @DisplayName("should create new message")
    void saveMessage() {
        // given
        final CreateMessageRequest requestBody = new CreateMessageRequest(
                "spring.boot@gmail.com",
                "ago",
                "deaf");

        // when
        final CreateMessageResponse response = testRestTemplate.postForObject(
                "http://localhost:" + port + "/api/messages", requestBody, CreateMessageResponse.class);

        // then
        final Message createdMessage = inMemoryMessageRepository.savedMessages().get(0);
        assertThat(response.id()).isEqualTo(createdMessage.id());
        assertThat(createdMessage.email()).isEqualTo(requestBody.email());
        assertThat(createdMessage.title()).isEqualTo(requestBody.title());
        assertThat(createdMessage.content()).isEqualTo(requestBody.content());
    }

    private static Stream<Arguments> messagesByEmail() {
        return Stream.of(
                Arguments.of("spring.boot@gmail.com", List.of(
                        new GetMessageByEmailResponse("spring.boot@gmail.com", "glory"),
                        new GetMessageByEmailResponse("spring.boot@gmail.com", "level")
                )),
                Arguments.of("boot.spring@gmail.com", emptyList())
        );
    }

    @TestConfiguration
    static class SimpleMessageApiTestContext {

        @Bean
        MessageRepository messageRepository() {
            return new InMemoryMessageRepository();
        }
    }
}
