package com.asseco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleMessageApi {

    public static void main(String[] args) {
        SpringApplication.run(SimpleMessageApi.class, args);
    }
}
