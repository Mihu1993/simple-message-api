package com.asseco.infrastructure.repository.cassandra;

import com.asseco.domain.Message;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Table("message")
@Data
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class MessageEntity {

    @PrimaryKey
    private MessageEntityKey key;
    @NotBlank
    private String title;
    @NotBlank
    private String content;

    public MessageEntity(final Message message) {
        this(new MessageEntityKey(message.id(), message.email()), message.title(), message.content());
    }

    public Message toMessage() {
        return new Message(key.id(), key.email(), title, content);
    }
}
