package com.asseco.infrastructure.repository.cassandra;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;

@Repository
public interface SpringDataCassandraMessageRepository
        extends CassandraRepository<MessageEntity, MessageEntityKey> {

    Stream<MessageEntity> findByKeyEmail(String email);
}
