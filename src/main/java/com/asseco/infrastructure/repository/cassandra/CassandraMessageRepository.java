package com.asseco.infrastructure.repository.cassandra;

import com.asseco.domain.Message;
import com.asseco.domain.repository.MessageRepository;

import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.stream.Stream;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class CassandraMessageRepository implements MessageRepository {

    private final SpringDataCassandraMessageRepository messageRepository;

    @Override
    public UUID save(final Message message) {
        final MessageEntity savedEntity =
                messageRepository.save(new MessageEntity(message));
        return savedEntity.key().id();
    }

    @Override
    public Stream<Message> findByEmail(final String email) {
        return messageRepository.findByKeyEmail(email).map(MessageEntity::toMessage);
    }
}
