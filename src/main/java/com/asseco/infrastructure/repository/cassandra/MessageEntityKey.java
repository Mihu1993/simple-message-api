package com.asseco.infrastructure.repository.cassandra;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import java.util.UUID;

import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@PrimaryKeyClass
@Data
@Accessors(fluent = true)
@AllArgsConstructor
@NoArgsConstructor
public class MessageEntityKey {

    @PrimaryKeyColumn
    private UUID id;
    @PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
    @Email
    private String email;
}
