package com.asseco.infrastructure.configuration;

import com.asseco.infrastructure.repository.cassandra.SpringDataCassandraMessageRepository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.repository.config.EnableCassandraRepositories;

@Configuration
@EnableCassandraRepositories(
        basePackageClasses = SpringDataCassandraMessageRepository.class)
public class CassandraConfiguration {
}
