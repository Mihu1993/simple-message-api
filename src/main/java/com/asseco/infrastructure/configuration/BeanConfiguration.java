package com.asseco.infrastructure.configuration;

import com.asseco.domain.repository.MessageRepository;
import com.asseco.domain.service.DomainMessageService;
import com.asseco.domain.service.MessageService;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {

    @Bean
    MessageService messageService(final MessageRepository messageRepository) {
        return new DomainMessageService(messageRepository);
    }
}
