package com.asseco.application.response;

import java.util.UUID;

public record CreateMessageResponse(UUID id) {
}
