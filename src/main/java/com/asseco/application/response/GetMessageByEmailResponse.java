package com.asseco.application.response;

public record GetMessageByEmailResponse(String email, String title) {
}
