package com.asseco.application.rest;

import com.asseco.application.request.CreateMessageRequest;
import com.asseco.application.response.CreateMessageResponse;
import com.asseco.application.response.GetMessageByEmailResponse;
import com.asseco.domain.Message;
import com.asseco.domain.service.MessageService;

import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

import javax.validation.constraints.Email;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@Validated
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @GetMapping(
            value = "/messages",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GetMessageByEmailResponse> findMessagesByEmail(
            @Email @RequestParam final String email) {
        return messageService.findMessagesByEmail(email).map(this::toResponse).toList();
    }

    @PostMapping(
            value = "/messages",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public CreateMessageResponse createMessage(
            @RequestBody final CreateMessageRequest createMessageRequest) {
        final UUID messageId =
                messageService.createMessage(toMessage(createMessageRequest));
        return new CreateMessageResponse(messageId);
    }

    private Message toMessage(final CreateMessageRequest createMessageRequest) {
        return new Message(
                UUID.randomUUID(),
                createMessageRequest.email(),
                createMessageRequest.title(),
                createMessageRequest.content());
    }

    private GetMessageByEmailResponse toResponse(final Message message) {
        return new GetMessageByEmailResponse(message.email(), message.title());
    }
}

