package com.asseco.application.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public record CreateMessageRequest(@Email String email, @NotBlank String title, @NotBlank String content) {
}
