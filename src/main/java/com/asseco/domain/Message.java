package com.asseco.domain;

import java.util.UUID;

public record Message(UUID id, String email, String title, String content) {
}
