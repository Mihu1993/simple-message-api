package com.asseco.domain.repository;

import com.asseco.domain.Message;

import java.util.UUID;
import java.util.stream.Stream;

public interface MessageRepository {

    UUID save(Message message);

    Stream<Message> findByEmail(String email);
}
