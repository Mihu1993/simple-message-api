package com.asseco.domain.service;

import com.asseco.domain.Message;

import java.util.UUID;
import java.util.stream.Stream;

public interface MessageService {

    UUID createMessage(Message message);

    Stream<Message> findMessagesByEmail(String email);
}
