package com.asseco.domain.service;

import com.asseco.domain.Message;
import com.asseco.domain.repository.MessageRepository;

import java.util.UUID;
import java.util.stream.Stream;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DomainMessageService implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    public UUID createMessage(final Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Stream<Message> findMessagesByEmail(final String email) {
        return messageRepository.findByEmail(email);
    }
}
